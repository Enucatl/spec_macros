# Custom SPEC macros

Macros for [Certif SPEC](http://www.certif.com/).
to load these macros automatically at SPEC startup on mpc1054, if this package is
installed in `~/custom_spec_macros`

    :::bash
    su -c 'echo "
    {
      #load custom user macros
      local user_dir
      user_dir = \"/home/specuser/custom_spec_macros\"

      qdofile(sprintf(\"%s/all_macros.mac\",user_dir))
    }
    " >> /usr/local/lib/spec.d/site.mac'

## all\_macros.mac

Just includes all the custom macros with `qdofile`.

## dmesh.mac

Delta mesh scan. The easiest way to explain it is to say that dmesh is to
mesh as dscan is to ascan.

    :::bash
    dmesh inner_loop_motor start finish intervals outer_loop_motor start finish intervals exposure_time

## mdpc\_tube\_comet.mac

Control the Comet high-energy tube. Copied from the backups of
mpc1054.psi.ch.

## mythen.mac

Control the Mythen detector integrating it in the ct command.
